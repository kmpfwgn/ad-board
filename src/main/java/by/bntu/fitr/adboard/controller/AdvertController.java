package by.bntu.fitr.adboard.controller;

import by.bntu.fitr.adboard.converter.AdvertConverter;
import by.bntu.fitr.adboard.model.AdvertModel;
import by.bntu.fitr.adboard.service.AdvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/adv")
public class AdvertController {

    @Autowired
    private AdvertConverter advertConverter;

    @Autowired
    private AdvertService advertService;

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<AdvertModel> findAll(){
        return advertConverter.getModel(advertService.getAll());
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public void save(@RequestBody AdvertModel advertModel){
        advertService.save(advertConverter.getEntity(advertModel));
    }

    @RequestMapping(value = "/author/{author}", method = RequestMethod.GET)
    public List<AdvertModel> getByAuthor(@PathVariable("author") String author){
        return advertConverter.getModel(advertService.getByAuthor(author));
    }
}
