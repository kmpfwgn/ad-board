package by.bntu.fitr.adboard.converter.impl;

import by.bntu.fitr.adboard.converter.AdvertConverter;
import by.bntu.fitr.adboard.entity.Advert;
import by.bntu.fitr.adboard.entity.User;
import by.bntu.fitr.adboard.model.AdvertModel;
import by.bntu.fitr.adboard.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdvertConverterImpl implements AdvertConverter {

    @Autowired
    private UserService userService;

    @Override
    public Advert getEntity(AdvertModel advertModel) {
        Advert advert = new Advert();
        advert.setBody(advertModel.getBody());
        advert.setHeader(advertModel.getHeader());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = sdf.parse(advertModel.getPublishingDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        advert.setPublishingDate(date);

        return advert;
    }

    @Override
    public List<Advert> getEntity(List<AdvertModel> advertModels) {
        List<Advert> adverts = new ArrayList<>();
        for (AdvertModel advertModel : advertModels) {
            adverts.add(getEntity(advertModel));
        }
        return adverts;
    }

    @Override
    public AdvertModel getModel(Advert advert) {
        AdvertModel advertModel = new AdvertModel();

        User u = userService.findById(advert.getAuthorID());
        advertModel.setAuthor(u.getUsername());

        advertModel.setBody(advert.getBody());
        advertModel.setHeader(advert.getHeader());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(advert.getPublishingDate());
        advertModel.setPublishingDate(date);

        return advertModel;
    }

    @Override
    public List<AdvertModel> getModel(List<Advert> adverts) {
        List<AdvertModel> advertModels = new ArrayList<>();
        for (Advert advert : adverts) {
            advertModels.add(getModel(advert));
        }
        return advertModels;
    }
}
