package by.bntu.fitr.adboard.converter;

import by.bntu.fitr.adboard.entity.Advert;
import by.bntu.fitr.adboard.model.AdvertModel;

import java.util.List;

public interface AdvertConverter {

    Advert getEntity(AdvertModel advertModel);

    List<Advert> getEntity(List<AdvertModel> advertModels);

    AdvertModel getModel(Advert advert);

    List<AdvertModel> getModel(List<Advert> adverts);
}
