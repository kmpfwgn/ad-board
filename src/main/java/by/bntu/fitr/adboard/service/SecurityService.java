package by.bntu.fitr.adboard.service;


public interface SecurityService {

    String findLoggedInUsername();

    String getUsername();

    void autoLogin(String username, String password);
}
