package by.bntu.fitr.adboard.service.impl;

import by.bntu.fitr.adboard.dao.AdvertDao;
import by.bntu.fitr.adboard.entity.Advert;
import by.bntu.fitr.adboard.entity.User;
import by.bntu.fitr.adboard.service.AdvertService;
import by.bntu.fitr.adboard.service.SecurityService;
import by.bntu.fitr.adboard.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AdvertServiceImpl implements AdvertService {

    @Autowired
    private AdvertDao advertDao;

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Override
    public List<Advert> getAll() {
        return advertDao.findAll();
    }

    @Override
    public void save(Advert advert) {
        String username = securityService.getUsername();
        User user = userService.findByUsername(username);

        advert.setAuthorID(user.getId());

        advertDao.save(advert);
    }

    @Override
    public List<Advert> getByAuthor(String username) {
        User user = userService.findByUsername(username);
        if (user == null){
            return new ArrayList<>();
        }
        return advertDao.getByAuthorID(user.getId());
    }
}
