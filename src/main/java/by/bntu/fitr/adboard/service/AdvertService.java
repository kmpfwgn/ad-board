package by.bntu.fitr.adboard.service;

import by.bntu.fitr.adboard.entity.Advert;

import java.util.List;

public interface AdvertService {

    List<Advert> getAll();

    void save(Advert advert);

    List<Advert> getByAuthor(String username);
}
