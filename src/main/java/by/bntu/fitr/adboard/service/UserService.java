package by.bntu.fitr.adboard.service;

import by.bntu.fitr.adboard.entity.User;


public interface UserService {

    void save(User user);

    User findByUsername(String username);

    User findById(Integer id);
}
