package by.bntu.fitr.adboard.dao;

import by.bntu.fitr.adboard.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleDao extends JpaRepository<Role, Long> {
}
