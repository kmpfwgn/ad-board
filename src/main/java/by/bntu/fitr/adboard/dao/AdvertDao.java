package by.bntu.fitr.adboard.dao;

import by.bntu.fitr.adboard.entity.Advert;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AdvertDao extends JpaRepository<Advert, Long> {

    List<Advert> getByAuthorID(Integer authorId);
}
