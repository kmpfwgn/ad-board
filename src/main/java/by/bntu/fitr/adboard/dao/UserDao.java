package by.bntu.fitr.adboard.dao;

import by.bntu.fitr.adboard.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findById(Integer id);
}
