<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create advert</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/bootstrap/css/bootstrap.css">
    <script type="text/javascript" src="${contextPath}/resources/bootstrap/js/bootstrap.js"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/styles.css">
</head>
<body>

    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-2"></div>
            <div class="col col-md-8 full-adv">
                <form>
                    <div class="c-element">
                        <h6>Enter header of your advert</h6>
                        <input id="inputHeader" type="text" placeholder="Enter header">
                    </div>
                    <div class="c-element">
                    <h6>Enter body of your advert</h6>
                    <textarea id="inputBody" placeholder="Enter body" rows="4"></textarea>
                    </div>
                    <br>
                    <a href="" id="saveButton">Save</a>
                </form>
            </div>
            <div class="col col-md-2"></div>
        </div>
    </div>

    <script type="text/javascript" src="${contextPath}/resources/js/createAdv.js"></script>
</body>
</html>