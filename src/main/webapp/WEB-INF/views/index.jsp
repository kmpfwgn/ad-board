<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Advert Board</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/bootstrap/css/bootstrap.css">
    <script type="text/javascript" src="${contextPath}/resources/bootstrap/js/bootstrap.js"></script>
    <link rel="stylesheet" type="text/css" href="${contextPath}/resources/css/styles.css">
</head>
<body>
    <div class="container-fluid my-header">
        <div class="row align-items-center justify-content-end">
            <div class="col col-md-auto c-element">
                <a href="myAdverts">My adverts</a>
            </div>
            <div class="col col-md-auto c-element">
                <a href="createAdv">Create advert</a>
            </div>
            <div class="col col-md-auto c-element">
                <a href="registration">Sign up</a>
            </div>
            <div class="col col-md-auto c-element">
                <a href="login">Sign in</a>
            </div>
        </div>
    </div>

    <br>

    <div class="container-fluid">
        <div class="row">
            <div class="col col-md-auto">
                <div class="c-element">
                    <p>Search by author</p>
                    <form>
                        <input type="text" id="searchAuthorId" placeholder="Enter author">
                        <a href="" id="searchByAuthor">Search</a>
                    </form>
                </div>
                <br>
            </div>

            <div class="col adv-area" id="advert-list">
            </div>
        </div>
    </div>

    <script src="${contextPath}/resources/js/main.js"></script>
</body>
</html>