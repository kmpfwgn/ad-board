$(document).ready(function () {

    $.ajax({
        type: 'GET',
        url: window.location.origin + '/api/adv/all',
        success: function (result) {
            fillAdverts(result);
        }
    });

    function fillAdverts(adverts) {
        $('#advert-list').empty();
        var advHtml = '<div class="container adv">';
        $.each(adverts, function (i, adv) {
            var pd = new Date(adv.publishingDate);
            var pdy = pd.getFullYear();
            var pdm = pd.getMonth() + 1;
            var pdd = pd.getDate();
            var header = '<h4>' + adv.header + '</h4>';
            var publishingDate = '<p>' + [pdy, pdm, pdd].join('-') + '</p>';
            var author = '<a href="">' + adv.author + '</a>';
            var body = '<p>' + adv.body + '</p>';
            $('#advert-list').append(advHtml + header + publishingDate + author + body + '</div>');
        })
    };

    $('#searchByAuthor').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'GET',
            url: window.location.origin + '/api/adv/author/' +
                document.getElementById("searchAuthorId").value,
            success: function (result) {
                fillAdverts(result)
            }
        });
    });
});
