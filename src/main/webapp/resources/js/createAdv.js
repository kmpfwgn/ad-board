$(document).ready(function(){

    $('#saveButton').click(function (event) {
        event.preventDefault();

        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: window.location.origin + '/api/adv/save',
            data: JSON.stringify({
                header: $('#inputHeader').val(),
                body: $('#inputBody').val(),
                publishingDate: getCurrentDate()
            }),
            success: function (result) {
            }
        });
    });

    function getCurrentDate(){
        var d = new Date();
        var month = d.getMonth() + 1;
        var day = d.getDate();
        var year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year,month,day].join('-')
    }
});
